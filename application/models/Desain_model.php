<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desain_model extends CI_Model {
      var $tabel = 'design';

    
 public function __construct()
    {
        parent::__construct();
        $this->load->database();
	}
     
public function listing(){
	$hasil = $this->db->get('design');
		if($hasil->num_rows() > 0){
			return $hasil->result();
		} else {
			return array();
		}

}   

			public function tambah($data){
	
			$this->db->insert('design',$data);
		
	}	

public function edit($data){
    
			$this->db->where('no',$data['no']);
	
			$this->db->update('design',$data);
		
	}	
public function delete($data){
    
		
			$this->db->where('no',$data['no']);
	
			$this->db->delete('design',$data);
		
		
	}	


public function detail($no){
		//Query mencari record berdasarkan ID-nya
		$this->db->select('*');
    $this->db->from('design');
        $this->db->where('no',$no);
    $this->db->order_by('no','asc');
    $query = $this->db->get();
    return $query->row();
			

}

    


}
