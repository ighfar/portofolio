<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
      var $tabel = 'user';

    
 public function __construct()
    {
        parent::__construct();
        $this->load->database();
	}
     
public function listing(){
	$hasil = $this->db->get('user');
		if($hasil->num_rows() > 0){
			return $hasil->result();
		} else {
			return array();
		}

}   

			public function tambah($data){
	
			$this->db->insert('user',$data);
		
	}	

public function edit($data){
    
			$this->db->where('id_user',$data['id_user']);
	
			$this->db->update('user',$data);
		
	}	
public function delete($data){
    	$this->db->where('id_user',$data['id_user']);
	
			$this->db->delete('user',$data);
		
	}	


public function detail($id_user){
		//Query mencari record berdasarkan ID-nya
		$this->db->select('*');
    $this->db->from('user');
        $this->db->where('id_user',$id_user);
    $this->db->order_by('id_user','desc');
    $query = $this->db->get();
    return $query->row();
			

}

public function login($username,$password){

		$this->db->select('*');
    $this->db->from('user');
        $this->db->where(array('username'=> $username,
        						'password' => sha1($password)));
    $this->db->order_by('id_user','desc');
    $query = $this->db->get();
    return $query->row();
			

}
    


}
