<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Identitas_model extends CI_Model {
      var $tabel = 'identitas';

    
 public function __construct()
    {
        parent::__construct();
        $this->load->database();
	}
     
public function listing(){
	$hasil = $this->db->get('identitas');
		if($hasil->num_rows() > 0){
			return $hasil->result();
		} else {
			return array();
		}

}   

			public function tambah($data){
	
			$this->db->insert('identitas',$data);
		
	}	

public function edit($data){
    
			$this->db->where('nama',$data['nama']);
	
			$this->db->update('identitas',$data);
		
	}	
public function delete($data){
    
		
			$this->db->where('nama',$data['nama']);
	
			$this->db->delete('identitas',$data);
		
		
	}	


public function detail($nama){
		//Query mencari record berdasarkan ID-nya
		$this->db->select('*');
    $this->db->from('identitas');
        $this->db->where('nama',$nama);
    $this->db->order_by('nama','desc');
    $query = $this->db->get();
    return $query->row();
			

}

    


}
