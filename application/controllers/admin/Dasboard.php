<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasboard extends CI_Controller {


	public function index()
	{
        $data = array('title' => 'Halaman Administrator',
                     'isi'    => 'admin/dasboard/list');
		$this->load->view('admin/layout/wrapper',$data,false);
	}
}
