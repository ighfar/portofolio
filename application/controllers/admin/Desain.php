<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desain extends CI_Controller {
    

    
 public function __construct()
    {
        parent::__construct();
       
        $this->load->model('desain_model');
    
 
               
	}

	public function index()
	{
        	 $desain = $this->desain_model->listing();
        $data = array('title' => 'Data Desain',
                      'desain'  => $desain,
                     'isi'    => 'admin/desain/list');
		$this->load->view('admin/layout/wrapper',$data,false);
	}
    public function tambah()
	{
     
        $valid = $this->form_validation;
  
        
      $valid->set_rules('tahun','Tahun','required',
                array('required' => '%s harus diisi'));
        
        if($valid->run()){
           $config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 2400;
		$config['max_width']            = 2024;
		$config['max_height']           = 2024;
            		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('gambar')){
 
        $data = array('title' => 'Tambah Desain',
                      'error' => $this->upload->display_errors(),
                     'isi'    => 'admin/desain/tambah');
		$this->load->view('admin/layout/wrapper',$data,false);
        }else{
            	$upload_gambar = array('upload_data' => $this->upload->data());
	
            $config['image_library'] = 'gd2';
$config['source_image'] = './uploads/'.$upload_gambar['upload_data']['file_name'];
            $config['new_image'] = './uploads/thumbs/';
$config['create_thumb'] = TRUE;
$config['maintain_ratio'] = TRUE;
$config['width']         = 250;
$config['height']       = 250;
$config['thumb_marker'] = '';
$this->load->library('image_lib', $config);

$this->image_lib->resize();
            $i = $this->input;
            $data = array(

                            'gambar' => $upload_gambar['upload_data']['file_name'],
                             'tahun' => $i->post('tahun')
                         );
            $this->desain_model->tambah($data);
            $this->session->set_flashdata('sukses','Data telah ditambah');
            redirect(base_url('admin/desain'),'refresh');
        }
        }
            
        $data = array('title' => 'Tambah Desain',
                    //  'error' => $this->upload->display_errors,
                     'isi'    => 'admin/desain/tambah');
		$this->load->view('admin/layout/wrapper',$data,false);
	}
        
        public function edit($no)
	{
		$desain = $this->desain_model->detail($no);
        $valid = $this->form_validation;
              
          $valid->set_rules('tahun','Tahun','required',
                array('required' => '%s harus diisi'));
        
     
        
        if($valid->run()){
        	if(!empty($_FILES['gambar']['name'])){
           $config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 2400;
		$config['max_width']            = 2024;
		$config['max_height']           = 2024;
            		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('gambar')){
 
        $data = array('title' => 'Edit Identitas',
                      'error' => $this->upload->display_errors(),
                     'isi'    => 'admin/desain/tambah');
		$this->load->view('admin/layout/wrapper',$data,false);
        }else{
            	$upload_gambar = array('upload_data' => $this->upload->data());
	
            $config['image_library'] = 'gd2';
$config['source_image'] = './uploads/'.$upload_gambar['upload_data']['file_name'];
            $config['new_image'] = './uploads/thumbs/';
$config['create_thumb'] = TRUE;
$config['maintain_ratio'] = TRUE;
$config['width']         = 250;
$config['height']       = 250;
$config['thumb_marker'] = '';
$this->load->library('image_lib', $config);

$this->image_lib->resize();
            $i = $this->input;
            $data = array('no' => $i->post('no'),
                         
                            'gambar' => $upload_gambar['upload_data']['file_name'],
                            'tahun' => $i->post('tahun')
                         );
            $this->identitas_model->edit($data);
            $this->session->set_flashdata('sukses','Data telah diedit');
            redirect(base_url('admin/desain'),'refresh');
        }}else{

 $i = $this->input;
            $data = array('no' => $i->post('no'),
                         
                           //'gambar' => $upload_gambar['upload_data']['file_name'],
                            'tahun' => $i->post('tahun')
                         );
            $this->desain_model->edit($data);
            $this->session->set_flashdata('sukses','Data telah diedit');
            redirect(base_url('admin/desain'),'refresh');
        }
        }
            
        $data = array('title' => 'Edit Desain',
        			'desain' => $desain,
                   //  'error' => $this->upload->display_errors,
                     'isi'    => 'admin/desain/edit');
		$this->load->view('admin/layout/wrapper',$data,false);
	}
    
        public function delete($no)
	{
     
    $data = array('no' => $no);        
                                                    
            $this->desain_model->delete($data);
            $this->session->set_flashdata('sukses','Data telah dihapus');
            redirect(base_url('admin/desain'),'refresh');
        
	}
    
}
