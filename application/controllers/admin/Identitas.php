<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Identitas extends CI_Controller {
    

    
 public function __construct()
    {
        parent::__construct();
       
        $this->load->model('identitas_model');
          //   $this->load->model('portfolio_model');
 
               
	}

	public function index()
	{
        	 $identitas = $this->identitas_model->listing();
        $data = array('title' => 'Data Identitas',
                      'identitas'  => $identitas,
                     'isi'    => 'admin/identitas/list');
		$this->load->view('admin/layout/wrapper',$data,false);
	}
    public function tambah()
	{
     
        $valid = $this->form_validation;
    
        $valid->set_rules('nama','nama','required',
                array('required' => '%s harus diisi'));
          $valid->set_rules('alamat','alamat','required',
                array('required' => '%s harus diisi'));
          $valid->set_rules('kontak','kontak','required',
                array('required' => '%s harus diisi'));
              $valid->set_rules('pekerjaan','pekerjaan','required',
                array('required' => '%s harus diisi'));
         $valid->set_rules('deskripsi','deskripsi','required',
                array('required' => '%s harus diisi'));
        
    
        
        if($valid->run()){
           $config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 2400;
		$config['max_width']            = 2024;
		$config['max_height']           = 2024;
            		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('gambar')){
 
        $data = array('title' => 'Tambah Identitas',
                      'error' => $this->upload->display_errors(),
                     'isi'    => 'admin/identitas/tambah');
		$this->load->view('admin/layout/wrapper',$data,false);
        }else{
            	$upload_gambar = array('upload_data' => $this->upload->data());
	
            $config['image_library'] = 'gd2';
$config['source_image'] = './uploads/'.$upload_gambar['upload_data']['file_name'];
            $config['new_image'] = './uploads/thumbs/';
$config['create_thumb'] = TRUE;
$config['maintain_ratio'] = TRUE;
$config['width']         = 250;
$config['height']       = 250;
$config['thumb_marker'] = '';
$this->load->library('image_lib', $config);

$this->image_lib->resize();
            $i = $this->input;
            $data = array('nama' => $i->post('nama'),
                          'alamat' => $i->post('alamat'),                   
                          'kontak' => $i->post('kontak'),         
                        'pekerjaan' => $i->post('pekerjaan'),
                            'deskripsi' => $i->post('deskripsi'),
                            'gambar' => $upload_gambar['upload_data']['file_name']
                         );
            $this->identitas_model->tambah($data);
            $this->session->set_flashdata('sukses','Data telah ditambah');
            redirect(base_url('admin/identitas'),'refresh');
        }
        }
            
        $data = array('title' => 'Tambah Identitas',
                    //  'error' => $this->upload->display_errors,
                     'isi'    => 'admin/identitas/tambah');
		$this->load->view('admin/layout/wrapper',$data,false);
	}
        
        public function edit($nama)
	{
		$identitas = $this->identitas_model->detail($nama);
        $valid = $this->form_validation;
               $valid->set_rules('nama','nama','required',
                array('required' => '%s harus diisi'));
          $valid->set_rules('alamat','alamat','required',
                array('required' => '%s harus diisi'));
          $valid->set_rules('kontak','kontak','required',
                array('required' => '%s harus diisi'));
          $valid->set_rules('pekerjaan','pekerjaan','required',
                array('required' => '%s harus diisi'));
        
     
        
        if($valid->run()){
        	if(!empty($_FILES['gambar']['name'])){
           $config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 2400;
		$config['max_width']            = 2024;
		$config['max_height']           = 2024;
            		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('gambar')){
 
        $data = array('title' => 'Tambah Identitas',
                      'error' => $this->upload->display_errors(),
                     'isi'    => 'admin/identitas/tambah');
		$this->load->view('admin/layout/wrapper',$data,false);
        }else{
            	$upload_gambar = array('upload_data' => $this->upload->data());
	
            $config['image_library'] = 'gd2';
$config['source_image'] = './uploads/'.$upload_gambar['upload_data']['file_name'];
            $config['new_image'] = './uploads/thumbs/';
$config['create_thumb'] = TRUE;
$config['maintain_ratio'] = TRUE;
$config['width']         = 250;
$config['height']       = 250;
$config['thumb_marker'] = '';
$this->load->library('image_lib', $config);

$this->image_lib->resize();
            $i = $this->input;
            $data = array('nama' => $i->post('nama'),
                          'alamat' => $i->post('alamat'),                   
                          'kontak' => $i->post('kontak'),         
                        'pekerjaan' => $i->post('pekerjaan'),
                            'deskripsi' => $i->post('deskripsi'),
                            'gambar' => $upload_gambar['upload_data']['file_name']
                         );
            $this->identitas_model->edit($data);
            $this->session->set_flashdata('sukses','Data telah diedit');
            redirect(base_url('admin/identitas'),'refresh');
        }}else{

 $i = $this->input;
            $data = array('nama' => $i->post('nama'),
                          'alamat' => $i->post('alamat'),                   
                          'kontak' => $i->post('kontak'),         
                        'pekerjaan' => $i->post('pekerjaan'),
                            'deskripsi' => $i->post('deskripsi'),
                         //   'gambar' => $upload_gambar['upload_data']['file_name']
                         );
            $this->identitas_model->edit($data);
            $this->session->set_flashdata('sukses','Data telah diedit');
            redirect(base_url('admin/identitas'),'refresh');
        }
        }
            
        $data = array('title' => 'Edit Identitas',
        			'identitas' => $identitas,
                   //  'error' => $this->upload->display_errors,
                     'isi'    => 'admin/identitas/edit');
		$this->load->view('admin/layout/wrapper',$data,false);
	}
    
        public function delete($nama)
	{
     
    $data = array('nama' => $nama);        
                                                    
            $this->identitas_model->delete($data);
            $this->session->set_flashdata('sukses','Data telah dihapus');
            redirect(base_url('admin/identitas'),'refresh');
        
	}
    
}
