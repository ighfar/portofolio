<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

 public function __construct()
    {
        parent::__construct();
       
        $this->load->model('identitas_model');
         $this->load->model('desain_model');
 
               
	}
	public function index()
	{
		 $identitas = $this->identitas_model->listing();
		 	 $desain = $this->desain_model->listing();
        $data = array('title' => 'PORTFOLIO IQBAL RIDHA',
                      'identitas' => $identitas,
                       'desain'  => $desain );
	$this->load->view('layout/wrapper',$data,false);
	}
}
