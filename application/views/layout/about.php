
        <!-- About Section-->
        <section class="page-section bg-primary text-white mb-0" id="about">
            <div class="container">
                <!-- About Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-white">About</h2>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div> 
                <!-- About Section Content-->
                <div class="row">
                    <div class="col-lg-8 ml-auto"><p class="lead"><?php echo $identitas->deskripsi;?></p>
                  </div>
                <!-- About Section Button-->
              
            </div>
        </section>