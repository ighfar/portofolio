<?php

if(isset($error)){
    echo '<p class="alert alert warning">';
    echo $error;
    echo '</p>';
}

echo validation_errors('<div class="alert alert-warning">','</div');

echo form_open_multipart(base_url('admin/desain/edit/'.$desain->no),'class="form horizontal"');

?> 


 <div class="form-group">
<label class="col-sm-2 control-label">No</label>
<div class="col-sm-10">
<input type="text" name="no" class="form-control" placeholder="no" value="<?php echo $desain->no ?>">
</div>
</div>
<br>
 <div class="form-group">
<label class="col-sm-2 control-label">Gambar</label>
<div class="col-sm-10">
<input type="file" name="gambar" class="form-control" placeholder="gambar" value="<?php echo $desain->gambar ?>">
</div>
</div>
<br>

 <div class="form-group">
<label class="col-sm-2 control-label">Tahun</label>
<div class="col-sm-10">
<input type="text" name="tahun" class="form-control" placeholder="tahun" value="<?php echo $desain->tahun ?>">
</div>
</div>
<br>
 <div class="form-group">

<div class="col-sm-10">
 <button type="submit" class="btn btn-md btn-success">Simpan</button>
              <button type="reset" class="btn btn-md btn-warning">reset</button>
</div>
</div>

<?php form_close(); ?>