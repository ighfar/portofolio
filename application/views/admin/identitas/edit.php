<?php

if(isset($error)){
    echo '<p class="alert alert warning">';
    echo $error;
    echo '</p>';
}

echo validation_errors('<div class="alert alert-warning">','</div');

echo form_open_multipart(base_url('admin/identitas/edit/'.$identitas->nama),'class="form horizontal"');

?> 
<div class="form-group">
<label class="col-sm-2 control-label">Nama</label>
<div class="col-sm-10">
<input type="text" name="nama" class="form-control" placeholder="nama" value="<?php echo $identitas->nama ?>">
</div> 
    
</div>
<br>

 <div class="form-group">
<label class="col-sm-2 control-label">Alamat</label>
<div class="col-sm-10">
<input type="text" name="alamat" class="form-control" placeholder="alamat" value="<?php echo $identitas->alamat ?>">
</div>     
</div>
<br>

 <div class="form-group">
<label class="col-sm-2 control-label">Kontak</label>
<div class="col-sm-10">
<input type="text" name="kontak" class="form-control" placeholder="kontak" value="<?php echo $identitas->kontak ?>">
</div>
</div>
<br>

 <div class="form-group">
<label class="col-sm-2 control-label">Pekerjaan</label>
<div class="col-sm-10">
<input type="text" name="pekerjaan" class="form-control" placeholder="pekerjaan" value="<?php echo $identitas->pekerjaan ?>">
</div>
</div>
<br>
 <div class="form-group">
<label class="col-sm-2 control-label">deskripsi</label>
<div class="col-sm-10">
<input type="text" name="deskripsi" class="form-control" placeholder="deskripsi" value="<?php echo $identitas->deskripsi ?>">
</div>
</div>
<br>
 <div class="form-group">
<label class="col-sm-2 control-label">Gambar</label>
<div class="col-sm-10">
<input type="file" name="gambar" class="form-control" placeholder="gambar" value="<?php echo $identitas->gambar ?>">
</div>
</div>
<br>

 <div class="form-group">

<div class="col-sm-10">
 <button type="submit" class="btn btn-md btn-success">Simpan</button>
              <button type="reset" class="btn btn-md btn-warning">reset</button>
</div>
</div>

<?php form_close(); ?>