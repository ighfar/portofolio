


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Simple_login 
{
	protected $CI;
	
	function __construct()
	{
		$this->CI =& get_instance();

		$this->CI->load->model('user_model');
	}

	public function login($username,$password){
		$check = $this->CI->user_model->login($username,$password);

		if ($check) {
			$id_user = $check->id_user;
			$nama = $check->nama;

			$this->CI->session->set_userdata('id_user',$id_user);
			$this->CI->session->set_userdata('nama',$nama);
			$this->CI->session->set_userdata('username',$username);

			redirect(base_url('admin/dasbor'),'refresh');

					}else{


			$this->CI->session->set_userdata('warning','username atau password salah');
					redirect(base_url('login'),'refresh');


					}
	}

	public function cek_login(){

		if($this->CI->session->set_userdata('username') == ""){

			$this->CI->session->set_userdata('warning','anda belum login');
					redirect(base_url('login'),'refresh');
		}

	}

	public function logout()
	{
		$this->CI->session->unset_userdata('id_user');
			$this->CI->session->unset_userdata('nama');
			$this->CI->session->unset_userdata('username');
						$this->CI->session->set_userdata('warning','anda berhasil logout');
					redirect(base_url('login'),'refresh');
		
	}
}